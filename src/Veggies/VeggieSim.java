package Veggies;

import java.util.ArrayList;
import java.util.List;


public class VeggieSim {

    
    public static void main(String[] args) {
        //Create 2 instances of Beet, 1 ripe, 1 not ripe.
        Beet b1 = new Beet("red", 2, "Ripe Beet");
        Beet b2 = new Beet("red", 1.2, "Not Ripe Beet");
        //Print results
        //System.out.println(b1.toString());
        //System.out.println("--------------");
        //System.out.println(b2.toString());
        
        //Create 2 instances of Carrot, 1 ripe, 1 not ripe
        Carrot c1 = new Carrot("orange", 1.5, "Ripe Carrot");
        Carrot c2 = new Carrot("orange", 0.5, "Not Ripe Carrot");
        
        //System.out.println(c1.toString());
       // System.out.println("--------------");
        //System.out.println(c2.toString());
        
        List<Vegetable> veggies = new ArrayList<Vegetable>();
        veggies.add(b1);
        veggies.add(b2);
        veggies.add(c1);
        veggies.add(c2);
        
        for(Vegetable veg : veggies){
            System.out.println(veg.toString());
        }
        
        
    }
    
}
