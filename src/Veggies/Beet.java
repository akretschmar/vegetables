
package Veggies;


public class Beet extends Vegetable{
    private String beetName;
    
public Beet(){}
    
public Beet(String colour, double size, String beetName){
    super(colour, size);
    this.beetName = beetName;
}

@Override
boolean isRipe(){
   boolean ripeness;
    if(colour == "red" && size == 2){
        ripeness = true;
    }
    else{ 
        ripeness = false;
    }
    return ripeness;
}

    @Override
    public String toString() {
        String out = "--------------------\n";
        out += beetName + "\n";
        out += "Colour: " + colour + "\n";
        out += "Size: " + size + "\n";
        out += "Ripe: " + isRipe()+ "\n";
        out += "--------------------\n";
        
        return out;
    }




}
