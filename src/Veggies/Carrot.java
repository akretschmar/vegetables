
package Veggies;


public class Carrot extends Vegetable {
    private String carrotName;
    
public Carrot(){}

public Carrot(String colour, double size, String carrotName){
    super(colour, size);
    this.carrotName = carrotName;
}

@Override
boolean isRipe(){
   boolean ripeness;
    if(colour == "orange" && size == 1.5){
        ripeness = true;
    }
    else{ 
        ripeness = false;
    }
    return ripeness;
}

    @Override
    public String toString() {
        String out = "--------------------\n";
        out += carrotName + "\n";
        out += "Colour: " + colour + "\n";
        out += "Size: " + size + "\n";
        out += "Ripe: " + isRipe() + "\n";
        out += "--------------------\n";
        
        return out;
    }
}
