package Veggies;


abstract class Vegetable {
     protected String colour;
     protected double size;
     
public Vegetable(){}

public Vegetable(String colour, double size){
    this.colour = colour;
    this.size = size;
}

    public String getColour() {
        return colour;
    }

    public double getSize() {
        return size;
    }

    abstract boolean isRipe();


}
